import React from "react";
import TransparentButton from "./TransparentButton";
import { FaTimes } from "react-icons/fa";

export default function Sidebar() {
  return (
    <div className="sidebar gradient-custom">
      <FaTimes className="sidebar--closeIcon" />
      <div className="sidebar--image px-5 pb-5">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          className="bi bi-person-circle"
          viewBox="0 0 16 16"
        >
          <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
          <path
            fill-rule="evenodd"
            d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
          />
        </svg>
      </div>
      <div className="sidebar--name">Joann Mary Varghese</div>
      <div className="sidebar--personalDetails">
        <div className="sidebar--personalDetailsItem emailid">
          <span className="sidebar--label">Email id</span>
          <span>joann@gmail.com</span>
        </div>
        <div class="sidebar--personalDetailsItem age">
          <span className="sidebar--label">Age</span>
          <span>21</span>
        </div>
        <div class="sidebar--personalDetailsItem skillset">
          <span className="sidebar--label">Skillsets</span>
          <span>C++, JS</span>
        </div>
        <div class="sidebar--personalDetailsItem gender">
          <span className="sidebar--label">Gender</span> <span>Female</span>
        </div>
        <div class="sidebar--personalDetailsItem country">
          <span className="sidebar--label">Country </span> <span>India</span>
        </div>
        <div class="sidebar--personalDetailsItem state">
          <span className="sidebar--label">State </span> <span>Kerala</span>
        </div>
        <div class="sidebar--personalDetailsItem dor">
          <span className="sidebar--label">Date of Registration</span>
          <span>Sept 6, 2021</span>
        </div>
      </div>
      <div className="sidebar--edit">
        <TransparentButton
          to="/edit"
          show="Edit Profile"
          className="sidebar--editProfile"
        />
        <TransparentButton
          to="/login"
          show="Logout"
          className="sidebar--logout"
        />
      </div>
    </div>
  );
}
