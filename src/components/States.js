//import React from "react";
import { State } from "country-state-city";

export default function states(isocode) {
  let states = [];
  const allStates = State.getStatesOfCountry(isocode);
  allStates.forEach((state) => {
    states.push(state);
  });
  return states;
}
