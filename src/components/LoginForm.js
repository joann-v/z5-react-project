import React from "react";
import BlueButton from "./BlueButton";
import { useNavigate } from "react-router-dom";

export default function LoginForm() {
  let navigate = useNavigate();

  const [loginFormData, setLoginFormData] = React.useState({
    email: "",
    password: "",
  });

  function handleChange(event) {
    setLoginFormData((prevFormData) => {
      const { name, value } = event.target;
      return {
        ...prevFormData,
        [name]: value,
      };
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    let userExists = false;
    let correctPassword = false;

    for (var i = 0; i < localStorage.length; i++) {
      let emailKey = localStorage.key(i);
      let user = JSON.parse(localStorage.getItem(emailKey));
      if (localStorage.key(i) === loginFormData.email) {
        userExists = true;
        if (user.password === loginFormData.password) {
          //Creating a session storage of the user
          sessionStorage.setItem(loginFormData.email, loginFormData.password);
          correctPassword = true;
          navigate("/home", { state: loginFormData });
        }
      }
    }

    if (!userExists) {
      alert("User doesn't exist");
      event.preventDefault();
    } else if (!correctPassword) {
      alert("Incorrect Password!");
      event.preventDefault();
    }
  }

  return (
    <form className="loginForm" onSubmit={handleSubmit}>
      <input
        type="email"
        placeholder="Email Address"
        className="form--input"
        required
        onChange={handleChange}
        name="email"
        value={loginFormData.email}
      />
      <input
        type="password"
        placeholder="Password"
        className="form--input"
        required
        onChange={handleChange}
        name="password"
        value={loginFormData.password}
      />
      <BlueButton to="/home" show="Submit" onClick={handleSubmit} />
    </form>
  );
}
