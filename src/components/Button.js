import React from "react";
import { Link } from "react-router-dom";

export default function Button(props) {
  return (
    <div>
      <button type="button" className="btn-lg">
        <Link to={props.to} className="link">
          {props.show}
        </Link>
      </button>
    </div>
  );
}
