import React from "react";
import IconPageButton from "./IconPageButton";
import PopupCard from "./PopupCard";

export default function Navbar(props) {
  return (
    <nav className="navbar gradient-custom text-white">
      <div className="navbar--sideText">Hey {props.currentUser.fullName},</div>
      <div className="buttonList">
        <PopupCard currentUser={props.currentUser} />
        <IconPageButton to="/edit" type="edit" />
        <IconPageButton to="/login" type="signout" />
      </div>
    </nav>
  );
}
