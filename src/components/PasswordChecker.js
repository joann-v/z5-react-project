// global variables that check if password contains the required characters
let lower_check = 0;
let upper_check = 0;
let number_check = 0;
let symbol_check = 0;

export default function PasswordChecker(data) {
  // regular expressions for the different conditions
  const lower = new RegExp("(?=.*[a-z])");
  const upper = new RegExp("(?=.*[A-Z])");
  const number = new RegExp("(?=.*[0-9])");
  const special = new RegExp("(?=.*[!@#$%&*])");

  // performing the checks

  lower_check = lower.test(data) ? 1 : 0;
  upper_check = upper.test(data) ? 1 : 0;
  number_check = number.test(data) ? 1 : 0;
  symbol_check = special.test(data) ? 1 : 0;

  if (number_check && symbol_check && lower_check && upper_check === 1)
    return true;
  else return false;
}
