import React from "react";
import { FaUser } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function IconButton() {
  function handleClick() {
    console.log("click");
  }
  return (
    <Link to="#" className="navbar--icon" onClick={handleClick}>
      <FaUser />
    </Link>
  );
}
