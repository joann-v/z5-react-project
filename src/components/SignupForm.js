import React from "react";
import BlueButton from "./BlueButton";
import countries from "./Countries";
import PasswordChecker from "./PasswordChecker";
import states from "./States";
import { useNavigate } from "react-router-dom";
import { DateTime } from "luxon";

export default function SignupForm() {
  let navigate = useNavigate();

  //to collect form data
  const [signupFormData, setSignupFormData] = React.useState({
    fullName: "",
    email: "",
    password: "",
    age: "",
    country: "",
    state: "",
    gender: "",
    isReact: false,
    isPython: false,
    isJava: false,
    dor: DateTime.now().toLocaleString(DateTime.DATETIME_MED),
  });

  //get countries data
  const countriesData = countries();
  const [statesData, setStatesData] = React.useState([{ name: "" }]);
  // create <option> elements for countries
  const countryElements = countriesData.map((country) => {
    return <option value={country.name}>{country.name}</option>;
  });
  // create <option> elements for states
  const stateElements =
    statesData &&
    statesData.map((state, index) => {
      if (index === 0 && statesData.length > 2) {
        return (
          <>
            <option value="">Select State</option>
            <option value={state.name}>{state.name}</option>
          </>
        );
      }
      return <option value={state.name}>{state.name}</option>;
    });

  // works when user makes changes to the form
  function handleChange(event) {
    setSignupFormData((prevFormData) => {
      const { name, value, type, checked } = event.target;

      if (name === "country") {
        countriesData.forEach((c) => {
          if (c.name === value) {
            //setting states data based on country selected
            let statesArray = states(c.isocode);
            setStatesData(statesArray);
          }
        });
      }
      return {
        ...prevFormData,
        [name]: type === "checkbox" ? checked : value,
      };
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    // checking if password contains required characters
    if (PasswordChecker(signupFormData.password)) {
      if (localStorage.getItem(signupFormData.email) === null) {
        //Updating a users details into the local storage
        localStorage.setItem(
          signupFormData.email,
          JSON.stringify(signupFormData)
        );
        //Creating a session storage of the user
        sessionStorage.setItem(signupFormData.email, signupFormData.password);
        navigate("/home", { state: signupFormData });
      } else {
        alert("User already exists!");
      }
    } else {
      alert("Password not strong enough");
    }
  }

  return (
    <form className="signupForm">
      <input
        type="text"
        placeholder="Full Name"
        className="form--input"
        required
        onChange={handleChange}
        name="fullName"
        value={signupFormData.fullName}
      />
      <input
        type="email"
        placeholder="Email Address"
        className="form--input"
        required
        onChange={handleChange}
        name="email"
        value={signupFormData.email}
      />
      <input
        type="password"
        placeholder="Password"
        className="form--input"
        required
        onChange={handleChange}
        name="password"
        value={signupFormData.password}
      />
      <input
        type="number"
        placeholder="Age"
        className="form--input"
        required
        min="0"
        max="100"
        onChange={handleChange}
        name="age"
        value={signupFormData.age}
      />
      <div className="form--select">
        <label className="text-muted">Country</label>
        <select
          onChange={handleChange}
          name="country"
          value={signupFormData.country}
          required
        >
          <option value="">Select Country</option>
          {countryElements}
        </select>
      </div>
      <div className="form--select">
        <label className="text-muted">State</label>
        <select
          onChange={handleChange}
          name="state"
          value={signupFormData.state}
          required
        >
          {stateElements}
        </select>
      </div>
      <div className="form--radio">
        <label className="text-muted">Gender</label>
        <div className="form--allRadio">
          <input
            type="radio"
            id="male"
            onChange={handleChange}
            name="gender"
            value="Male"
            checked={signupFormData.gender === "Male"}
          />
          <label htmlFor="male">Male</label>
          <input
            type="radio"
            id="female"
            onChange={handleChange}
            name="gender"
            value="Female"
            checked={signupFormData.gender === "Female"}
          />
          <label htmlFor="female">Female</label>
          <input
            type="radio"
            id="other"
            onChange={handleChange}
            name="gender"
            value="Other"
            checked={signupFormData.gender === "Other"}
          />
          <label htmlFor="other">Other</label>
        </div>
      </div>
      <div className="form--checkbox">
        <label className="text-muted">Skills</label>
        <div className="form--allCheckbox">
          <input
            type="checkbox"
            id="isReact"
            name="isReact"
            checked={signupFormData.isReact}
            onChange={handleChange}
          />
          <label htmlFor="isReact">REACT</label>

          <input
            type="checkbox"
            id="isPython"
            name="isPython"
            checked={signupFormData.isPython}
            onChange={handleChange}
          />
          <label htmlFor="isPython">Python</label>

          <input
            type="checkbox"
            id="isJava"
            name="isJava"
            checked={signupFormData.isJava}
            onChange={handleChange}
          />
          <label htmlFor="isJava">Java</label>
        </div>
      </div>
      <BlueButton to="/home" show="Sign Up" onClick={handleSubmit} />
    </form>
  );
}
