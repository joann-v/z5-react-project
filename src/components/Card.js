import React from "react";
import { FaEnvelope, FaPencilRuler, FaMapMarkerAlt } from "react-icons/fa";

export default function Card(props) {
  let react = props.skills[0] ? "React " : "";
  let python = props.skills[1] ? "Python " : "";
  let java = props.skills[2] ? "Java" : "";
  let skillsGiven = props.skills.includes(true);

  return (
    <div className="card">
      <div className="card--container gradient-custom text-white">
        <h4 className="card--header">
          <b>{props.name}</b>
        </h4>
        <ul className="card--listGroup">
          <div className="card--listGroupDiv">
            <FaEnvelope />
            <li className="card--listGroupItem email">{props.email}</li>
          </div>
          {skillsGiven ? (
            <div className="card--listGroupDiv">
              <FaPencilRuler />
              <li className="card--listGroupItem skill">
                {react} {python} {java}
              </li>
            </div>
          ) : (
            <></>
          )}
          <div className="card--listGroupDiv">
            <FaMapMarkerAlt />
            <li className="card--listGroupItem location">
              {props.country}, {props.state}
            </li>
          </div>
        </ul>
      </div>
    </div>
  );
}
