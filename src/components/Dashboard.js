import React from "react";
import Card from "../components/Card";

export default function Dashboard(props) {
  const cardElements = props.empData.map((emp) => {
    return (
      <Card
        name={emp.fullName}
        email={emp.email}
        skills={[emp.isReact, emp.isPython, emp.isJava]}
        country={emp.country}
        state={emp.state}
      />
    );
  });

  return (
    <div className="dashboard">
      <div className="dashboard--heading">
        Welcome to the Employee Directory
      </div>
      <div className="dashboard--directory">{cardElements}</div>
    </div>
  );
}
