import React from "react";
import { FaEdit, FaSignOutAlt } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function IconPageButton(props) {
  function handleLogout() {
    sessionStorage.clear();
  }
  if (props.type === "edit") {
    return (
      <Link to={props.to} className="navbar--icon">
        <FaEdit />
      </Link>
    );
  } else {
    return (
      <Link to={props.to} className="navbar--icon" onClick={handleLogout}>
        <FaSignOutAlt />
      </Link>
    );
  }
}
