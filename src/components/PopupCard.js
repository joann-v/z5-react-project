import React from "react";
import Popup from "reactjs-popup";
import { FaUser } from "react-icons/fa";
import { Link } from "react-router-dom";

export default function PopupCard(props) {
  let react = props.currentUser.isReact ? "React " : "";
  let python = props.currentUser.isPython ? "Python " : "";
  let java = props.currentUser.isJava ? "Java" : "";
  let skills = [
    props.currentUser.isReact,
    props.currentUser.isPython,
    props.currentUser.isJava,
  ];
  let skillsGiven = skills.includes(true);

  return (
    <Popup
      trigger={
        <Link to="#" className="navbar--icon">
          <FaUser />
        </Link>
      }
      modal
      nested
    >
      {(close) => (
        <div className="modal gradient-custom text-white">
          <button className="close" onClick={close}>
            &times;
          </button>
          <div className="modal--content">
            <div className="modal--header"> Profile </div>
            <table className="modal--table">
              <caption className="table--caption">
                {props.currentUser.fullName}
              </caption>
              <tr>
                <td className="table--label">Email id</td>
                <td className="table--result">{props.currentUser.email}</td>
              </tr>
              <tr>
                <td className="table--label">Age</td>
                <td className="table--result">{props.currentUser.age}</td>
              </tr>
              {skillsGiven ? (
                <tr>
                  <td className="table--label">Skillsets</td>
                  <td className="table--result">
                    {react} {python} {java}
                  </td>
                </tr>
              ) : (
                <></>
              )}
              <tr>
                <td className="table--label">Gender</td>
                <td className="table--result">{props.currentUser.gender}</td>
              </tr>
              <tr>
                <td className="table--label">Country</td>
                <td className="table--result">{props.currentUser.country}</td>
              </tr>
              <tr>
                <td className="table--label">State</td>
                <td className="table--result">{props.currentUser.state}</td>
              </tr>
              <tr>
                <td className="table--label">Date of Registration</td>
                <td className="table--result">{props.currentUser.dor}</td>
              </tr>
            </table>
          </div>
        </div>
      )}
    </Popup>
  );
}
