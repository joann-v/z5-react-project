import { Navigate, Outlet } from "react-router-dom";

export default function PrivateRoutes() {
  return sessionStorage.key(0) === null ? (
    <Navigate to={"/login"} />
  ) : (
    <Outlet />
  );
}
