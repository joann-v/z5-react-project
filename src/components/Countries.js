//import React from "react";
import { Country } from "country-state-city";

export default function countries() {
  let countries = [];
  const allCountries = Country.getAllCountries();
  allCountries.forEach((country) => {
    countries.push({ name: country.name, isocode: country.isoCode });
  });
  return countries;
}
