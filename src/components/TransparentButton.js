import React from "react";
import { Link } from "react-router-dom";

export default function TransparentButton(props) {
  return (
    <div>
      <button type="button" className="tsp-btn-lg">
        <Link to={props.to} className="text-white link">
          {props.show}
        </Link>
      </button>
    </div>
  );
}
