import React from "react";

export default function Button(props) {
  return (
    <div>
      <button type="button" className="blue-btn-lg" onClick={props.onClick}>
        {props.show}
      </button>
    </div>
  );
}
