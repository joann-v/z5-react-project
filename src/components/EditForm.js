import React from "react";
import BlueButton from "./BlueButton";
import PasswordChecker from "./PasswordChecker";
import { useNavigate } from "react-router-dom";

export default function EditForm(props) {
  let navigate = useNavigate();

  let currentUser = props.currentUser;
  const [editFormData, setEditFormData] = React.useState({
    fullName: currentUser.fullName,
    email: currentUser.email,
    password: currentUser.password,
    age: currentUser.age,
    country: currentUser.country,
    state: currentUser.state,
    gender: currentUser.gender,
    isReact: currentUser.isReact,
    isPython: currentUser.isPython,
    isJava: currentUser.isJava,
    dor: currentUser.dor,
  });

  function handleChange(event) {
    setEditFormData((prevFormData) => {
      const { name, value, type, checked } = event.target;

      return {
        ...prevFormData,
        [name]: type === "checkbox" ? checked : value,
      };
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    // checking if password contains required characters
    if (PasswordChecker(editFormData.password)) {
      //Updating a users details into the local storage
      localStorage.setItem(editFormData.email, JSON.stringify(editFormData));
      //Creating a session storage of the user
      sessionStorage.setItem(editFormData.email, editFormData.password);
      navigate("/home", { state: editFormData });
    } else {
      alert("Password not strong enough");
    }
  }

  return (
    <form className="editForm">
      <input
        type="text"
        placeholder="Full Name"
        className="form--input"
        disabled
        name="fullName"
        value={editFormData.fullName}
      />
      <input
        type="email"
        placeholder="Email Address"
        className="form--input"
        disabled
        name="email"
        value={editFormData.email}
      />
      <input
        type="password"
        placeholder="Password"
        className="form--input"
        onChange={handleChange}
        name="password"
        value={editFormData.password}
      />
      <input
        type="number"
        placeholder="Age"
        className="form--input"
        disabled
        name="age"
        value={editFormData.age}
      />
      <div className="form--select">
        <label className="text-muted">Country</label>
        <select name="country" value={editFormData.country} disabled>
          <option value="">{editFormData.country}</option>
        </select>
      </div>
      <div className="form--select">
        <label className="text-muted">State</label>
        <select name="state" value={editFormData.state} disabled>
          <option value="">{editFormData.state}</option>
        </select>
      </div>
      <div className="form--radio">
        <label className="text-muted">Gender</label>
        <div className="form--allRadio">
          <input
            type="radio"
            value="Male"
            id="male"
            name="gender"
            checked={editFormData.gender === "Male"}
            disabled
          />
          <label for="male">Male</label>
          <input
            type="radio"
            value="Female"
            id="female"
            name="gender"
            checked={editFormData.gender === "Female"}
            disabled
          />
          <label for="female">Female</label>
          <input
            type="radio"
            value="Other"
            id="other"
            name="gender"
            checked={editFormData.gender === "Other"}
            disabled
          />
          <label for="other">Other</label>
        </div>
      </div>
      <div className="form--checkbox">
        <label className="text-muted">Skills</label>
        <div className="form--allCheckbox">
          <input
            type="checkbox"
            id="isReact"
            name="isReact"
            checked={editFormData.isReact}
            onChange={handleChange}
          />
          <label for="react">REACT</label>

          <input
            type="checkbox"
            id="isPython"
            name="isPython"
            checked={editFormData.isPython}
            onChange={handleChange}
          />
          <label for="python">Python</label>

          <input
            type="checkbox"
            id="isJava"
            name="isJava"
            checked={editFormData.isJava}
            onChange={handleChange}
          />
          <label for="java">Java</label>
        </div>
      </div>
      <BlueButton to="/home" show="Edit" onClick={handleSubmit} />
    </form>
  );
}
