import React from "react";

export default function PageNotFound() {
  return <h1 className="pageNotFound text-white">This Page Does Not Exist!</h1>;
}
