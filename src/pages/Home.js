import React from "react";
//import Sidebar from "../components/Sidebar";
import Dashboard from "../components/Dashboard";
import Navbar from "../components/Navbar";

export default function Home() {
  let currentUserKey = sessionStorage.key(0);
  let currentUser = JSON.parse(localStorage.getItem(currentUserKey));
  let empData = [];
  for (let i = 0; i < localStorage.length; i++) {
    if (localStorage.key(i) !== currentUserKey) {
      empData.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
    }
  }

  return (
    <div className="home">
      <Navbar currentUser={currentUser} />
      <Dashboard empData={empData} />
    </div>
  );
}
