import React from "react";
import LoginForm from "../components/LoginForm";
import { Link } from "react-router-dom";

export default function Login() {
  return (
    <div className="login login__card">
      <h5 className="card-title">Login</h5>
      <LoginForm />
      <div className="sub-card">
        <div className="text-muted">Don't have an account?</div>
        <Link to="/signup" className="card-link">
          Sign Up
        </Link>
      </div>
    </div>
  );
}
