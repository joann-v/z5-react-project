import React from "react";
import Button from "../components/Button";

export default function Welcome() {
  return (
    <div className="welcome">
      <div className="welcome__content">
        <h1 className="welcome__h1">Employee Directory</h1>
        <Button to="/login" show="LOGIN" />
        <Button to="/signup" show="SIGN UP" />
      </div>
    </div>
  );
}
