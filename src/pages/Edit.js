import React from "react";
import EditForm from "../components/EditForm";
import { Link } from "react-router-dom";

export default function Edit() {
  let currentUserKey = sessionStorage.key(0);
  let currentUser = JSON.parse(localStorage.getItem(currentUserKey));

  return (
    <div className="edit edit__card">
      <Link to="/home" className="edit--back">
        Back to Dashboard
      </Link>
      <h5 className="card-title">Edit Profile</h5>
      <EditForm currentUser={currentUser} />
    </div>
  );
}
