import React from "react";
import SignupForm from "../components/SignupForm";
import { Link } from "react-router-dom";

export default function Signup() {
  return (
    <div className="signup signup__card">
      <h5 className="card-title">Sign Up</h5>
      <SignupForm />
      <div className="sub-card">
        <div className="text-muted">Already have an account?</div>
        <Link to="/login" className="card-link">
          Login Here
        </Link>
      </div>
    </div>
  );
}
